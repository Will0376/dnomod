package ru.will0376.dnomod.mixin;

import net.minecraft.entity.passive.EntityCow;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(EntityCow.class)
public class EntityCowMixin {
    @Inject(remap = false,at= @At("HEAD"),cancellable = true,method = {"getLivingSound","func_70639_aQ"})
    public void getLivingSound(CallbackInfoReturnable<String> cir) {
        cir.setReturnValue("dnomod:dno");
    }
    @Inject(remap = false,at= @At("HEAD"),cancellable = true,method = {"getHurtSound","func_70621_aR"})
    public void getHurtSound(CallbackInfoReturnable<String> cir) {
        cir.setReturnValue("dnomod:dno");

    }
    @Inject(remap = false,at= @At("HEAD"),cancellable = true,method = {"getDeathSound","func_70673_aS"})
    public void getDeathSound(CallbackInfoReturnable<String> cir) {
        cir.setReturnValue("dnomod:dno");
    }
}
