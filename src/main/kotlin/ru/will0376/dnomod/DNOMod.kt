package ru.will0376.dnomod

import cpw.mods.fml.common.Mod
import cpw.mods.fml.common.Mod.EventHandler
import cpw.mods.fml.common.event.FMLInitializationEvent
import cpw.mods.fml.common.event.FMLPostInitializationEvent
import cpw.mods.fml.common.event.FMLPreInitializationEvent
import cpw.mods.fml.common.event.FMLServerStartingEvent

@Mod(modid = "dnomod", version = "1.0.0", name = "DNOMod", modLanguage = "java", acceptableRemoteVersions = "*")
class DNOMod {

    companion object {
        @Mod.Instance
        var INSTANCE: DNOMod? = null
    }

    @EventHandler
    fun preInit(event: FMLPreInitializationEvent) {
        val modLog = event.modLog
    }

    @EventHandler
    fun init(event: FMLInitializationEvent) {
    }

    @EventHandler
    fun postInit(event: FMLPostInitializationEvent) {
    }

    @EventHandler
    fun onServerStart(event: FMLServerStartingEvent) {
    }

}